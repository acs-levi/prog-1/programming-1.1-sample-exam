package unique;

import java.util.Random;

public class Unique {
	public static void main(String[] args) {
		final int SIZE = 15; // length of arrays
		final int MAX = 10; //highest number to generate
		int[] original = new int[SIZE];
		int[] unique = new int[SIZE];
		// TODO 2.1 Fill the first array (original) with randomly chosen numbers from 1 to MAX (both inclusive)
		// TODO 2.2 Copy the integers of original to the second array (unique), but replace doubles with a zero
		// Your code HERE >>>


		Random r = new Random();
		boolean found;

		for (int i = 0; i < original.length; i++) {
			original[i] = r.nextInt(MAX) + 1;
			found = false;

			for (int j : unique) {
				if (original[i] == j) {
					found = true;
					break;
				}
			}

			if (!found)
				unique[i] = original[i];
		}

		// <<< Do not change the code below to print out the arrays
		System.out.print("Original: ");
		for (int i : original) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.print("Unique  : ");
		for (int i : unique) {
			if (i != 0) System.out.print(i + " ");
		}
	}

}
