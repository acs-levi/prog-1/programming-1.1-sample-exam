package movies;

public class Movie {

    private final String title;
    private final int year;

    public Movie(String title, int year) {
        this.title = title;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public String toString() {
        return String.format("%-30s" + year, title) ;
    }
}
