package vowels;

import java.util.Scanner;

public class Vowels {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input;
        int numberOfVowels = 0;

        System.out.print("Please enter a sentence: ");
        input = sc.nextLine();

        for (int i = 0; i < input.length(); i++) {
            switch(input.charAt(i)) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    numberOfVowels++;
                    break;
                default:
                    break;
            }
        }

        System.out.printf("The sentence contains %d vowels", numberOfVowels);
        sc.close();
    }
}
